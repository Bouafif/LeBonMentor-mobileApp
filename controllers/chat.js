/* const Conversation = require('../models/conversation'),
    Message = require('../models/message'),
    User = require('../models/user');
console.log("chat ctrl");

exports.getConversations = function(req, res) {
    // Only return one message from each conversation to display as snippet
    let id = req.params.connected_user_id
    Conversation.find({ participants: id })
        .select('_id')
        .exec(function(err, conversations) {
            if (err) {
                res.send({ error: err });

            }

            // Set up empty array to hold conversations + most recent message
            let fullConversations = [];
            conversations.forEach(function(conversation) {
                Message.find({ 'conversationId': conversation._id })
                    .sort('-createdAt')
                    .limit(1)
                    /* .populate({
                      path: "author",
                      select: "profile.firstName profile.lastName"
                    }) 
                    .exec(function(err, message) {
                        if (err) {
                            res.send({ error: err });

                        }
                        fullConversations.push(message);
                        if (fullConversations.length === conversations.length) {
                            return res.status(200).json({ conversations: fullConversations });
                        }
                    });
            });
        });
}

exports.getConversation = function(req, res) {
    console.log("entred get conversation", req.params.conversationId);

    Message.find({ conversationId: req.params.conversationId })
        .select('createdAt body author')
        .sort('-createdAt')
        /* .populate({
          path: 'author',
          select: 'profile.firstName profile.lastName'
        }) 
        .exec(function(err, messages) {
            if (err) {
                res.send({ error: err });

            }
            console.log("list messages", messages);

            res.status(200).json({ conversation: messages });
        });
}
exports.newConversation = function(req, res) {
    console.log("entred", req.params.recipient);

    if (!req.params.recipient) {
        res.status(422).send({ error: 'Please choose a valid recipient for your message.' });

    }

    if (!req.body.composedMessage) {
        res.status(422).send({ error: 'Please enter a message.' });

    }

    const conversation = new Conversation({
        participants: [req.user._id, req.params.recipient]
    });

    conversation.save(function(err, newConversation) {
        if (err) {
            res.send({ error: err });

        }

        const message = new Message({
            conversationId: newConversation._id,
            body: req.body.composedMessage,
            author: req.user._id
        });

        message.save(function(err, newMessage) {
            if (err) {
                res.send({ error: err });

            }

            res.status(200).json({ message: 'Conversation started!', conversationId: conversation._id });

        });
    });
}

exports.sendReply = function(req, res) {
    const reply = new Message({
        conversationId: req.params.conversationId,
        body: req.body.composedMessage,
        author: req.user._id
    });

    reply.save(function(err, sentReply) {
        if (err) {
            res.send({ error: err });

        }

        res.status(200).json({ message: 'Reply successfully sent!' });
    });
}

// DELETE Route to Delete Conversation
exports.deleteConversation = function(req, res) {
    Conversation.findOneAndRemove({
        $and: [
            { '_id': req.params.conversationId }, { 'participants': req.user._id }
        ]
    }, function(err) {
        if (err) {
            res.send({ error: err });

        }

        res.status(200).json({ message: 'Conversation removed!' });

    });
}

// PUT Route to Update Message
exports.updateMessage = function(req, res) {
    Conversation.find({
        $and: [
            { '_id': req.params.messageId }, { 'author': req.user._id }
        ]
    }, function(err, message) {
        if (err) {
            res.send({ error: err });

        }

        message.body = req.body.composedMessage;

        message.save(function(err, updatedMessage) {
            if (err) {
                res.send({ error: err });

            }

            res.status(200).json({ message: 'Message updated!' });

        });
    });
} */