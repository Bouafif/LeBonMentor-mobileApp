const mongoose = require('mongoose');
const titlize = require('mongoose-title-case');
const config = require('../config/db');

const MatiereSchema = new mongoose.Schema({

// Matiere model
creatorId: String,
creatorName: String,
subjectTitle: {
    type: String,
    required:true
},
subjectdescription: {
    type: String,
    required:true
},
subjectImage: {
    type: String,
},
subjectPrice: {
    type: String,
    required:true,
},
subjectLangage: {
    type: String,
    required:true,
},

// Courses
courses:[{
    courseTitle: String,
    courseDesc: String,
  courseFile: String,
  }

    ,{timestamps: true}
    ],

// exercices
exercices:[{
    
    exerciceTitle:String, 
    exerciceDesc: String,
    exerciceFile: String,
    exerciceReturnDate: Date,
    }
    ,{timestamps: true}
    ],

// Exams
exams:[
    
    examTitle={
        type: String,
        required:true,
    },
    examDesc={
        type: String,
        required:true,
    },
  examFile={
    type: String,
    required:true,
  },
  examReturnDate={
      type:Date,
      required:true
  }

    ,{timestamps: true}
    ]

    }) 







const Matiere = module.exports = mongoose.model('matiere', MatiereSchema);


//FIND COURSE BY COURSE_ID
module.exports.findByMatiere = function (type, callback) {
Matiere.find({_id: type }, callback).sort({createdAt:'desc'});
}

// //   FIND COURSE BY MENTOR'S ID
//   module.exports.findByMentor = function (type, callback) {
//     Cours.find({createdByid: type }, callback);
//   }
//  // // FIND COURSE BY MENTOR S ID
// module.exports.findByMentor = function (type, callback) {
// Cours.find({createdByid: type }, callback).sort({createdAt:'desc'});
// }






