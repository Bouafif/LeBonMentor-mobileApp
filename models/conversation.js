const mongoose = require('mongoose');
const titlize = require('mongoose-title-case');
const config = require('../config/db');

// Schema defines how chat messages will be stored in MongoDB
const ConversationSchema = new mongoose.Schema({
    participants: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
});

module.exports = mongoose.model('Conversation', ConversationSchema);