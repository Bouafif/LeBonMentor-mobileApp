const express = require('express');
const path = require('path');
var bodyParser = require('body-parser')
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/db');
const routeUser = require('./routes/userRoute');
const chat = require('./routes/chat');
const routeReservation = require('./routes/reservationRoute');
const routeMatiere = require('./routes/matiereRoute');
const app = express();

var http = require('http').Server(app);
var io = require('socket.io')(http);
//const io = require('socket.io').listen(server);




// Use Node's default promise instead of Mongoose's promise library
mongoose.Promise = global.Promise;

// Connect to the database
mongoose.connect(config.db);
let db = mongoose.connection;

db.on('open', () => {
    console.log('Connected to the database.');
});

db.on('error', (err) => {
    console.log('Database error: ' + err);
});




// Set body parser middleware
app.use('/uploads', express.static('uploads'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    res.append('Content-Type', 'application/form-data');
    next();
});
app.use('/api/user', routeUser);
app.use('/api/reservation', routeReservation);
app.use('/api/matiere', routeMatiere);
app.use('/api/chat', chat);
//


/* var http = require('http').Server(app);
var io = require('socket.io')(http);
 */


// Set public folder using built-in express.static middleware
app.use(express.static('public'));

app.get('/', function(req, res, next) {
    res.json("hello express")
});

// Use passport middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);


// Use express's default error handling middleware
app.use(function(err, req, res, next) {

    if (res.headersSent) {
        return next(err);
    }
    res.status(400).json({ err: err });
});


// Start the server

var port = process.env.PORT || 4200;

var server = app.listen(port, function() {
    console.log('Express server listening on port ' + port);
});