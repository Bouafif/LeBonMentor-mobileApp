const ChatController = require('./controllers/chat'),
    express = require('express'),
    app = express(),
    chatRoutes = express.Router();

exports = module.exports = function(io) {
    // Set socket.io listeners.
    io.on('connection', (socket) => {
        console.log('a user connected');

        // On conversation entry, join broadcast channel
        socket.on('enter conversation', (conversation) => {
            socket.join(conversation);
            console.log("in socket event", conversation)
                // ChatController.get('/' + conversation, ChatController.getConversation);
                // Retrieve single conversation
                // app.use('/', ChatController.getConversation)
                //chatRoutes.get('/api/chat' + conversation, ChatController.getConversation);

        });

        socket.on('leave conversation', (conversation) => {
            socket.leave(conversation);
            console.log('left ' + conversation);
        })

        socket.on('new message', (conversation) => {
            io.sockets.in(conversation).emit('refresh messages', conversation);
            console.log('new message  ' + conversation);
        });

        socket.on('disconnect', () => {
            console.log('user disconnected');
        });
    });
}