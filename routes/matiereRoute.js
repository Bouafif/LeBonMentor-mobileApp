// import { extname } from ('path');
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt-nodejs');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/db');
const Matiere = require('../models/matiere');
const Reservation= require('../models/reservation');
var bodyParser = require('body-parser')
const matId =('');
const multer = require('multer');
var testCours=[];
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, 'subjectsUploads/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
//   if (file.length === 0){      
//     ' subjectImage'= req.body.subjectImage
// }else
// {
//   ' subjectImage'= req.file.path


  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'|| file.mimetype === 'application/pdf' ){
  
    cb(null, true);
  }
  else {
    cb(null, false);

  
};
}
const upload = multer({
    storage: storage,
    // fileFilter: fileFilter
  });
  


// ADD SUBJECT
router.post("/addSubject",upload.single('subjectImage'),(req, res) => {
  console.log(req.body);
  
    const newMatiere = new Matiere({

        'creatorId':req.body.creatorId,
        'subjectTitle': req.body.subjectTitle,
        'subjectdescription': req.body.subjectdescription,
        'subjectImage':req.body.subjectImage || req.file.path ,
        'subjectPrice':req.body.subjectPrice,
        'subjectLangage':req.body.subjectLangage
    });
    newMatiere
      .save()
      .then(result => {
        console.log(result);
        res.status(201).json({
          message: "Subject successfully created",
          createdCourse: (result)
  })
       })
      })



// // // GET ALL COURSES 
// router.get("/getCourses", (req, res) => {
//     Cours.find() ,function (err, cours) {
//           if (err) return res.status(500).send("There was a problem finding the courses.");
//             res.status(200).send(cours);
        
//       }
//     });
  

    
//   //GETS SUBJECTS BY SUBJECT_ID
router.get('/getSubjectbyId/:subjectId', function (req, res){
  Matiere.findByMatiere(req.params.subjectId, function (err, matiere) {
      if (err) return res.status(500).send("There was a problem finding the subject.");
      res.status(200).send(matiere);
      });
  
  });




// UPDATES A SINGLE SUBJECT IN THE DATABASE

// ADD COURSE
router.put('/newCourse/:id',upload.single('courseFile'),function (req, res) { 
  
  Matiere.findByIdAndUpdate(req.params.id,
    {
      $push: {
        courses: 
         {
               "courseTitle": req.body.courseTitle,
               "courseDesc": req.body.courseDesc,
               "courseFile": req.file.path,

              
             } //inserted data is the object to be inserted 
    }
  
  }, 
    function(err, Model){
     
      if(err){
  console.log(err);
    }else{
        //console.log(Model);
        return res.status(200).json(Model);
        
    }
      
    }
  );
  })
// ADD EXERCICE

  router.put('/addExercice/:id',upload.single('exerciceFile'),function (req, res) { 
    Matiere.findByIdAndUpdate(req.params.id,
      {
        $push: {
          exercices: 
           {
            "exerciceTitle":req.body.exerciceTitle, 
            "exerciceDesc": req.body.exerciceDesc,
            "exerciceFile": req.body.path,
            "exerciceReturnDate": req.body.exerciceReturnDate    
            
                
               } //inserted data is the object to be inserted 
      }
    
    }, 
      function(err, Model){
       
        if(err){
    console.log(err);
      }else{
          //console.log(Model);
          return res.status(200).json(Model);
          
      }
        
      }
   );
   })


  //  ADD EXAM
   router.put('/addExam/:id',upload.single('exerciceFile'),function (req, res) { 
    Matiere.findByIdAndUpdate(req.params.id,
      {
        $push: {
          exams: 
           {
            "examTitle":req.body.examTitle, 
            "examDesc": req.body.examDesc,
            "examFile": req.body.path,
            "examReturnDate": req.body.examReturnDate    
            
                
               } //inserted data is the object to be inserted 
      }
    
    }, 
      function(err, Model){
       
        if(err){
    console.log(err);
      }else{
          //console.log(Model);
          return res.status(200).json(Model);
          
      }
        
      }
   );
   })
  //GETS COURSES BY STUDENT'S_ID
  router.get('/getCoursebyStudent/:studentId', function (req, res){

    Reservation.find({studentId:req.params.studentId}).then(value =>{
      for (let index = 0; index < value.length; index++) {
       
     // value.forEach(element => {
        Matiere.findByMatiere(value[index].matiereId,function (err, result) {
          if (err) return res.status(500).send("There was a problem finding the reserved course.");
          res.json(result);
          });

          

      }
      })

      
   
    })




//   //GETS COURSES BY MENTOR'S_ID
// router.get('/getCoursebyMent/:mentorId', function (req, res){
//   Matiere.findCourseByMentor(req.params.mentorId, function (err, cours) {
//       if (err) return res.status(500).send("There was a problem finding the course.");
//       res.status(200).send(cours);
//       });
//   });
  



    
      

module.exports = router;

