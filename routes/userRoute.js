const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt-nodejs');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/db');
const User = require('../models/user');
var bodyParser = require('body-parser')




// REGISTER
router.post('/register', (req, res) => {

    if (req.body.password !== req.body.passwordconfirm) {
        res.json({ success: false, msg: 'Passwords do not match.' });

    } else {

        let newUser = new User({
            name: req.body.name,
            email: req.body.email,
            username: req.body.username,
            typeofuser: req.body.typeofuser,
            password: req.body.password,
            // gender:req.body.gender,
            // birthday:req.body.birthday,
            // phonenum:req.body.phonenum,
            aboutme: req.body.aboutme,
            // Nameofclass1:req.body.Nameofclass1,
            // descriptionofclass1:req.body.descriptionofclass1,
            // priceofclass1:req.body.priceofclass1,
            // langageofclass1:req.body.langageofclass1
            // class2:req.body.class2,
            // class3:req.body.class3,

        });

        newUser.save((err) => {

            if (err) {
                if (err.errors) {


                    // if (err.errors. class2.Name) {
                    //   return res.json({ success: false, msg: err.errors. class2.Name.message });
                    //  // return;
                    // }
                    // if (err.errors. class2.description) {
                    //   return res.json({ success: false, msg: err.errors. class2.description.message });
                    //  // return;
                    // }
                    // if (err.errors. class3.Name) {
                    //   return res.json({ success: false, msg: err.errors. class3.Name.message });
                    //  // return;
                    // }
                    // if (err.errors. class3.description) {
                    //   return res.json({ success: false, msg: err.errors. class3.description.message });
                    //  // return;
                    // }



                    // Show failed if all else fails for some reasons
                    res.json({ success: false, msg: 'Failed to register.' });
                }
            } else {
                res.json({ success: true, msg: 'User successfully registered.' });
            }
        });


    }


});




// GET_ALL_USERS
// RETURNS ALL THE USERS IN THE DATABASE
router.get('/getUsers', function(req, res) {
    User.find({}, function(err, users) {
        if (err) return res.status(500).send("There was a problem finding the users.");
        res.status(200).send(users);
    });
});

// GETS A SINGLE USER FROM THE DATABASE
router.get('/getUserbyId/:id', function(req, res) {
    // console.log(req.params.id)
    User.findById(req.params.id, function(err, user) {
        if (err) return res.status(500).send("There was a problem finding the user.");
        res.status(200).send(user);
    });
});

// GETS A SINGLE USER BY HIS/HER TYPE FROM THE DATABASE
router.get('/getUserbyType/:userType', function(req, res) {
    console.log(req.params.userType)
    User.findByTypeofuser(req.params.userType, function(err, user) {
        if (err) return res.status(500).send("There was a problem finding the user.");
        if (!user) return res.status(404).send("No user found.");
        res.status(200).send(user);
    });
});

// GETS USERS WITH FILTERS FROM THE DATABASE
router.post('/UsersFilters', function(req, res) {
    console.log(req.body)
    User.findByFilterSearch(req.body, function(err, user) {
        if (err) return res.status(500).send({ status: "There was a problem finding the mentor." });
        if (user.length == 0) return res.status(404).send({ status: "No mentor found." });
        res.status(200).send({ mentors: user });
    });
});

// DELETES A USER FROM THE DATABASE
router.delete('/:id', function(req, res) {
    User.findByIdAndRemove(req.params.id, function(err, user) {
        if (err) return res.status(500).send("There was a problem deleting the user.");
        res.status(200).send("User: " + user.name + " was deleted.");
    });
});




// LOGIN
router.post('/login', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    User.findOne({ username: username }, (err, user) => {
        if (err) throw err;
        if (!user) {
            return res.json({ success: false, msg: 'User not found.' });
        }
        bcrypt.compare(password, user.password, function(err, isMatch) {
            if (err) throw err;

            if (isMatch) {
                const token = jwt.sign(user, config.secret, {
                    expiresIn: 604800 // 1 week
                });
                // Don't include the password in the returned user object
                return res.json({
                    success: true,
                    token: 'JWT ' + token,
                    user: user
                });
            } else {
                return res.status(200).json({ success: false, msg: 'Wrong password.' });
            }
        });
    });
});




//UPDATE MENTOR'S INFO AFTER CREATING A SUBJECT
router.put('/subject/updateMentor/:id', function(req, res) {
    console.log(req.params.id)
    console.log(req.body);


    User.findByIdAndUpdate(req.params.id, {
            $push: {
                subjectesList: {
                    'subjectId': req.body.subjectId,
                    'subjectTitle': req.body.subjectTitle,
                    'subjectdescription': req.body.subjectdescription,
                    'subjectImage': req.body.subjectImage,
                    'subjectPrice': req.body.subjectPrice,
                    'subjectLangage': req.body.subjectLangage
                }

                // //  Courses
                //  courses:{
                //      "courseTitle":req.body.courseTitle,
                //      "courseDesc":req.body.course.courseDesc,
                //     "courseFile":req.body.courseFile,
                //  }

                //        ,

                //   //  exercices
                //    exercices:{

                //        "exerciceTitle":req.body.exerciceTitle, 
                //        "exerciceDesc":req.body.exerciceTitle,
                //        "exerciceFile":req.body.exerciceFile,
                //        "exerciceReturnDate": req.body.exerciceReturnDate,
                //        }
                //        ,

                //   //  Exams
                //    exams:{
                //        "examTitle":req.body.examTitle,
                //        "examDesc":req.body.examDesc,
                //        "examFile":req.body.examFile,
                //        "examReturnDate":req.body.examReturnDate,

                //    }

                // }
            }
        },
        function(err, Model) {
            console.log(Model);

            if (err) {
                console.log(err);
            } else {
                console.log(Model);
                return res.status(200).json(Model);

            }

        }
    );
})

module.exports = router;