const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt-nodejs');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/db');
const Reservation = require('../models/reservation');
var bodyParser = require('body-parser')

// CREATES A NEW RESERVATION
router.post('/newReservation', function (req, res) {
    const reservation = new Reservation({
        matiereId: req.body.matiereId,
        studentId: req.body.studentId
    });

    // Save Reservation in the database
    reservation.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Reservation."
        });

})
})


//GETS A RESERVATION BY STUDENT_ID
router.get('/getReservation/:connectedId', function (req, res){
Reservation.findReservation(req.params.connectedId, function (err, reservation) {
    if (err) return res.status(500).send("There was a problem finding the reservation.");
    res.json(reservation);
    });
});
module.exports = router;
