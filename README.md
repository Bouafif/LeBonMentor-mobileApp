# le-bon-mentor-api

A RESTful back-end login starter kit using Node.js, Express.js, and MongoDB

Install dependencies
```
npm install
```

Run
```
npm start
```
